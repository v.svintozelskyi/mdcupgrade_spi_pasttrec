library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pasttrec_test_module is
    generic (
        chipid  : std_logic_vector(1 downto 0)
    );
    port(
        SCK : in std_logic;
        SDO : in std_logic;
        SDI : out std_logic := 'Z';
        RST : in std_logic
    );
end entity;

architecture tb of pasttrec_test_module is
    signal bitcounter   : integer range 0 to 25;
    --signal datavector   : std_logic_vector(18 downto 0);

    signal rstcounter   : integer range 0 to 7;
    signal isok         : std_logic;
    signal rw           : std_logic;
    signal addr         : std_logic_vector(3 downto 0);
    signal data         : std_logic_vector(7 downto 0);

    type fsm_t  is (IDLE, CMD, RESET,RESET2);
    signal fsm : fsm_t;

    type ram_t is array(0 to 15) of std_logic_vector(7 downto 0);
    signal ram : ram_t;
begin

    FSM_PROC: process
    begin
        wait until rising_edge(SCK);
        SDI <= 'Z';
        case fsm is
            when RESET =>
                if RST = '1' then
                    fsm <= RESET2;
                end if;
            when RESET2 =>
                if rstcounter = 0 then
                    fsm <= IDLE;
                    bitcounter <= 0;
                    ram <= (others => x"00");
                else
                    rstcounter <= rstcounter - 1;
                end if;
            when IDLE =>
                if SDO = '1' then
                    fsm <= CMD;
                    bitcounter <= 1;
                    isok <= '1';
                end if;
            when CMD  =>
                bitcounter <= bitcounter + 1;
                if (bitcounter = 1 and SDO /= '0')
                    or (bitcounter = 2 and SDO /= '1')
                    or (bitcounter = 3 and SDO /= '0')
                then
                    isok <= '0';
                    fsm <= IDLE;
                elsif (bitcounter = 4 and SDO /= chipid(1))
                    or (bitcounter = 5 and SDO /= chipid(0))
                then
                    isok <= '0';
                elsif bitcounter = 6 then
                    rw <= SDO;
                elsif bitcounter = 7 then
                    addr(3) <= SDO;
                elsif bitcounter = 8 then
                    addr(2) <= SDO;
                elsif bitcounter = 9 then
                    addr(1) <= SDO;
                elsif bitcounter = 10 then
                    addr(0) <= SDO;
                    if isok = '1' then
                        SDI <= '0';
                    end if;
                end if;
                if bitcounter >= 11 and bitcounter <= 18 and isok = '1' then
                    data(11+7-bitcounter) <= SDO;
                    if rw = '0' then
                        SDI <= SDO;
                        ram(to_integer(unsigned(addr)))(11+7-bitcounter) <= SDO;
                    else
                        SDI <= ram(to_integer(unsigned(addr)))(11+7-bitcounter);
                    end if;
                end if;
                if bitcounter >= 19 and bitcounter <= 21 and isok = '1' then
                    SDI <= '0';
                end if;
                if bitcounter = 19 and SDO /= '0' and isok = '1' then
                    report "Incorrect end. Should be zero.";
                end if;
                if bitcounter >= 20 then
                    if SDO = '1' then
                        bitcounter <= 1;
                        isok <= '1';
                    else
                        fsm <= IDLE;
                        bitcounter <= 0;
                    end if;
                end if;
        end case;
        if RST = '0' then
            fsm <= RESET;
            rstcounter <= 5;
        end if;
    end process FSM_PROC;
end architecture;
