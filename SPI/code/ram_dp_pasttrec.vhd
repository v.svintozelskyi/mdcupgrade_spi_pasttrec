LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_ARITH.ALL;
USE IEEE.std_logic_UNSIGNED.ALL;

use std.textio.all;

library work;
use work.trb_net_std.all;

entity ram_dp_pasttrec is
  generic(
    depth : integer := 2;
    width : integer := 4;
    initfile : string := ""
    );
  port(
    CLK   : in  std_logic;
    wr1   : in  std_logic;
    a1    : in  std_logic_vector(depth-1 downto 0);
    dout1 : out std_logic_vector(width-1 downto 0);
    din1  : in  std_logic_vector(width-1 downto 0);
    a2    : in  std_logic_vector(depth-1 downto 0);
    dout2 : out std_logic_vector(width-1 downto 0);
    a3    : in  std_logic_vector(depth-1 downto 0);
    dout3 : out std_logic_vector(width-1 downto 0)
    );
end entity;

architecture ram_dp_arch of ram_dp_pasttrec is
    type ram_t is array(0 to 2**depth-1) of std_logic_vector(width-1 downto 0);

    impure function read_from_file(filename : string) return ram_t is
        file text_file      : text open read_mode is filename;
        variable text_line  : line;
        variable mem        : ram_t;
        variable c : character;
        variable offset : integer;
        variable hex_val : std_logic_vector(3 downto 0);
    begin
        for i in 0 to 2**depth-1 loop
            readline(text_file, text_line);
            --hread(text_line, mem(i));

            offset := 0;

            while offset < mem(i)'high loop
            read(text_line, c);

            case c is
                when '0' => hex_val := "0000";
                when '1' => hex_val := "0001";
                when '2' => hex_val := "0010";
                when '3' => hex_val := "0011";
                when '4' => hex_val := "0100";
                when '5' => hex_val := "0101";
                when '6' => hex_val := "0110";
                when '7' => hex_val := "0111";
                when '8' => hex_val := "1000";
                when '9' => hex_val := "1001";
                when 'A' | 'a' => hex_val := "1010";
                when 'B' | 'b' => hex_val := "1011";
                when 'C' | 'c' => hex_val := "1100";
                when 'D' | 'd' => hex_val := "1101";
                when 'E' | 'e' => hex_val := "1110";
                when 'F' | 'f' => hex_val := "1111";

                when others =>
                hex_val := "XXXX";
                assert false report "Found non-hex character '" & c & "'";
            end case;

            mem(i)(mem(i)'high - offset
                downto mem(i)'high - offset - 3) := hex_val;
            offset := offset + 4;

            end loop;

        end loop;
        return mem;
    end function;

    SIGNAL ram : ram_t := read_from_file(initfile);
--  signal ram : std_logic_vector(2**depth*width-1 downto 0)  := content;
begin


  process(CLK)
    begin
      if rising_edge(CLK) then
        if a1 /= x"00" then
            if wr1 = '1' then
                ram(conv_integer(a1))   <= din1;
                dout1                   <= din1;
            else
                dout1 <= ram(conv_integer(a1));
            end if;
        else
            dout1 <= x"00000000";
        end if;

        if a2 /= x"00" then
            dout2 <= ram(conv_integer(a2));
        else
            dout2 <= x"00000000";
        end if;
        if a3 /= x"00" then
            dout3 <= ram(conv_integer(a3));
        else
            dout3 <= x"00000000";
        end if;
      end if;
    end process;

end architecture;
