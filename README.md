
# mdcupgrade_spi_pasttrec

Top level module for SPI communication with PASTTREC chips: 

> SPI/code/pasttrec_spi.vhd

## Hierarchy 

* loader.vhd
  + *MEMORY*: 			**ram_dp_pasttrec.vhd**				- updated version of ram_dp.vhd
  + *SPI_INTERFACE*:    **spi_ltc2600_pasttrec.vhd**   - updated version of spi_ltc2600.vhd                               

## Top-level module instantiation

The maximum number of connected PASTTREC chips to the whole system is 16. Every PASTTREC chip within one SPI bunch should have a unique chip id.

### Generic map
| Name        | Type        								| Default |  Description                         |
| ----------- | ----------  								| --- 			| ----------------------------------- |
| SPI_BUNCHES | integer range 1 to 4 	| 1			| Number of SPI bunches connected to the module		|
| SPI_PASTTREC_PER_BUNCH  | integer range 1 to 4 | 4 | Number of PASTTREC chips connected to each bunch |
| SPI_CHIP_IDs   | std_logic_vector(31 downto 0) | | Chip ids of connected  PASTTRECs|

The last parameter SPI_CHIP_IDs should be filled with all chip ids used in a system. The two least significant bytes set the id for first PASTTREC in the first SPI bunch, while the SPI_CHIP_IDs(2k + 1 downto 2k) sets chips id for PASTTREC #k+1.

Example: 2 SPI bunches with 2 PASTTRECs in each. First bunch’ chip ids: 00 01. Second bunch’ chip ids: 01 10. Then SPI_CHIP_IDs must be filled with x"000000" & “10010100”.
### Port  map
| Name        | Type       | Mode | Description                         |
| ----------- | ---------- | ---- | ----------------------------------- |
| CLK         | std_logic  | In   | Clock signal                        |
| BUS_RX      | CTRLBUS_RX | In   | Slow control input                  |
| BUS_TX      | CTRLBUS_TX | Out  | Slow control output                 |
| RST_IN      | std_logic  | In   | Reset                               |
| SPI_CS_OUT  | std_logic_vector(*SPI_BUNCHES-1* downto *0*)  | Out  | SPI chip-select signal. Active-low. |
| SPI_SDI_IN  | std_logic_vector(*SPI_BUNCHES-1* downto *0*)  | In   | SPI serial data in signal.          |
| SPI_SDO_OUT | std_logic_vector(*SPI_BUNCHES-1* downto *0*)  | Out  | SPI serial data out signal.         |
| SPI_SCK_OUT | std_logic_vector(*SPI_BUNCHES-1* downto *0*)  | Out  | SPI clock out signal.               |
| SPI_RST_OUT | std_logic_vector(*SPI_BUNCHES-1* downto *0*)  | Out  | SPI reset signal. Active-low.       |

## Memory

Whole system shares one common memory, implemented in ram_dp_pasttrec.vhd. For test purposes, it's content is loaded from file during compilation.

| Address     | Content                                                      | Description                                                  |
| ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 0x00  | 0x0000| Zero word. Can't be overided.|
| 0x01  | - | SPI single word buffer. Forbidden for writing during transmitting. **Not for end user** |
| 0x02  | **PASTTREC #2**<br>*30* - autoload enable<br>*29..24* - number of autoload commands (unsigned)<br>*23..16* - first command address<br>**PASTTREC #1**<br>*14* - autoload enable<br/>*13..8* - number of autoload commands (unsigned)<br/>*7..0* - first command address | Autostart settings for PASTTREC #1 & PASTTREC #2<br><br>Example: *0x00004C10* will trigger loading 12 commands starting from memory adress 0x10 for PASTTREC #1 *(it's black_settings_pt15_g1_thr127 set)*            |
| up to 0x02 + ⌈N/2⌉ - 1	| **PASTTREC #2k**<br/>*30* - autoload enable<br/>*29..24* - number of autoload commands (unsigned)<br/>*23..16* - first command address<br/>**PASTTREC #2k-1**<br/>*14* - autoload enable<br/>*13..8* - number of autoload commands (unsigned)<br/>*7..0* - first command address	|	Autostart settings for PASTTREC #2k-1 & PASTTREC #2k <br>*N - total number of PASTTREC chips connected to all SPI bunches*|
| ⌈N/2⌉ - 0xFF | -  | Free memory. Can be used by end user.<br>*N - total number of PASTTREC chips connected to all SPI bunches*  |

### Predefined sets of commands
There are 3 predefined sets of commands already in memory
* *0x10*: **default PASTTREC settings**   *(14 commands)*
* *0x20*: **black_settings_pt15_g1_thr127**   *(12 commands)*
* *0x30*: **black_settings_pt20_g1_thr127**  *(12 commands)*
* *0x40*: **blue_settings_pt10_g1_thr127**   *(12 commands)*

### Example of storing set in memory 
##### Format of commands in memory
| bits | meaning | is required |
| - | - | - |
| *18..15* | header | yes |
| *14..13* | chip id | in most cases no, as it will be overwritten during transmitting |
| *12*  | *1* - read mode<br>*0* - write mode | yes |
| *11..8* | PASTTREC register address | yes |
| *7..0* | data| yes |
##### First few commands for set *black_settings_pt15_g1_thr127*
| address | data |
| -- | -- |
| *0x20* | *0x00050019* |
| *0x21* | *0x0005011e* |
| *0x22* | *0x00050215* |
| *0x23* | *0x0005037f* |
| *0x24* | *0x0005040f* |
| ... | ... |



## Slow control

### General rules

* For every command, the module should respond with either ack or nack or unknown bit not later than 5 clock cycles.

* If second commands arrives earlier than module send response for first one, it'll be ignored.
* In a whole system, the PASTTREC chips are identified by a number _k_ within a range 0 to SPI_BUNCHES x SPI_PASTTREC_PER_BUNCH - 1. The exact chip id for every PASTTREC is assigned as SPI_CHIP_IDs(2*k* + 1 downto 2*k*)

### Protocol

| BUS_RX.addr                                       | Access mode | BUX_RX.data (only in W mode)                                 | BUX_TX.data                                                  | Description                                                  |
| ------------------------------------------------- | ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **0xA0\*\***                                      | -----       | ------------------------                                     | ----------------                                             | **Memory access**                                            |
| **0xA0** & addr[7..0]                             | R/W         | *31..0* - content to be written to memory cell *addr[7..0]*. | *31..0* - content of memory cell *addr[7..0*].               | Memory access<br>**Access to *0xA001* is forbidden during SPI transmitting.** |
| **0xA1\*\***                                      | -----       | ------------------------                                     | ----------------                                             | **SPI module access**                                        |
| **0xA100**                                        | W           | *31* - reset bit. High - active.                             | -                                                            | Set SPI module fsm reset high.<br>*It's bit 31 of ctrl_reg register* |
| **0xA101**                                        | R/W         | *31..0* - content to be written to SPI module ctrl_reg.<br>*Bits 6 and 7 can be high if and only if at least one of bits 5..0 is high.* | *31..0* - content of SPI module ctrl register                | Full access to SPI control register.                         |
| **0xA102**                                        | R/W         | *5* - override sdo <br>*4* - override sck <br/>*3* - override cs <br/>*2* - invert sdo <br/>*1* - invert sck <br/>*0* - invert cs | *5* - override sdo <br/>*4* - override sck <br/>*3* - override cs <br/>*2* - invert sdo <br/>*1* - invert sck <br/>*0* - invert cs | Access to override/invert register.<br>*It's bits 13..8 of ctrl_reg register* |
| **0xA103**                                        | R/W         | *15..6* - wait cycles (unsigned, def = 7).<br>*5..0* - word length (unsigned, def = 19) | *15..6* - wait cycles (unsigned, def = 7).<br/>*5..0* - word length (unsigned, def = 19) | Access to wait cycles/word length register.<br>*It's bits 29..14 of ctrl_reg register* |
| **0xA10A**                                        | W           | *7* - block bit. If high SPI module will block after transaction.<br>*6* - start bit. If high SPI module will begin transmitting.<br>*5..0* - number of words to be transmitted. Required to be non zero for transmitting (unsigned) | -                                                            | Transmit data direct from memory. <br>**Not for end user.**  |
| **0xA10B**                                        | R           | -                                                            | *31..0* - content of SPI readback register.                  | Read readback registed of SPI module. Can contain up to 4 last PASTTREC responces. Disable SPI blocking.<br> |
| **0xA2\*\***                                      | -----       | ------------------------                                     | ----------------                                             | **PASTTREC access.**                                         |
| **0xA2** & chip_num[3..0] & reg_no[3..0]    | R/W           | *7..0* - content to be written to PASTTREC register *reg_no[3..0]*. | -                                                            | PASTTREC chip registers access.<br>**If R mode the second slow control access is required**|
| **0xAA\*\***                                      | -----       | ------------------------                                     | ----------------                                             | **Complex operations.**                                      |
| **0xAA00**                                        | W           | -                                                            | -                                                            | Whole system reset.<br>Execute reset & autostart sequence of PASTTREC. |
| **0xAA** & chip_num[3..0] & all_bit & **001** | W           | *13..8* - number of commands in set. (unsigned, should be at least 1)<br>*7..0* - address of first command in memory. | -                                                            | Load set of commands from memory to PASTTREC  chip. **Bits 14..13 of command will be overwritten with actual chip_id before transmitting.**<br>If all_bit is high, set will be loaded to all PASTTREC chips.<br>If all_bit is low, set will be loaded only to PASTTREC number = chip_num[3..0]. |
| **0xAA** & chip_num[3..0] & all_bit & **002** | W           | *11..8* - PASTTREC register.<br>*7..0* - data to be written to register. | -                                                            | Edit only one PASTTREC register. <br>If all_bit is high, edit specified register for all PASTTREC chips.<br>If all_bit is low, edit specified register for only PASTTREC with number = chip_num[3..0]. |

## Reading data from PASTTREC

To read content of PASTTREC register it's required to send two slow control requests:
* **0xA2\*\*** - Chip register access for loading content of PASTTREC register to readback SPI register.
* **0xA10B** - Readback register reading

## Reset & autostart sequence

During the initial load, the module will skip reset sequence and in few clock cycles will make the transition to IDLE state. The last one allows R/W access to the main memory. **Neither SPI module nor PASTTREC is working here!**
After all necessary changes to memory are complete, a user should send a *0xA000* command to the module. This will begin a usual reset sequence.

During reset sequence firstly the module will send two words with low and high RST_OUT values for resetting all PASTTREC chips.

![image](https://i.ibb.co/7kqqP1n/reset.png)

After this, the module will begin transmitting the configuration commands for every PASTTREC chip one by one, first for chip #1. It can be configured with memory registers 0x02, etc. **For every particular command, the chip_id will be overwritten with the actual one!**

## Files

Code:

> SPI/code/\*

Simulation files:

> SPI/sim/\*
> SPI/syssim/\*

